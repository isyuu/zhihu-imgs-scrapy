## 知乎图片下载工具
专治知乎钓鱼帖子的爬虫工具,哈哈哈.
增加了cookies缓存功能.

#### 准备/依赖
```
python 3.6
pip install requests
pip install selenium
pip install beautifulsoup4

pip install pyinstaller [选装,打包成exe使用]
```


#### 爬取全部question图片
```
python crawl.py -chrome [ChromeDriver](http://chromedriver.chromium.org/)工具 -url 知乎问题地址例如:https://www.zhihu.com/question/26037846
ps:windows/osx chromedriver 已经下载好了, 在webbrowers目录下

亦可以直接使用交互式命令:
python crawl.py 

或者使用 pyinstaller -F crawl.py 打包成exe给非开发环境使用.

```